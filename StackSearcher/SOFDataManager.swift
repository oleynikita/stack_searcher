//
//  SOFDataManager.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/12/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

enum DataMangerError: Error {
    case downloadError
    case decodingError
}

class SOFDataManager {
    let jsonDecoder = JSONDecoder()
    
    let baseUrl = "https://api.stackexchange.com/2.2"
    
    
    private func searchQuestionsUrl(q: String, page: Int = 1) -> URL {
        let page = String(page)
        let searchPrefix = "/search/advanced?"
        let params = [
            "page" : page,
            "pagesize" : "30",
            "order" : "desc",
            "sort" : "activity",
            "q" : q,
            "site" : "stackoverflow",
            "filter" : "!-*f(6rc.bTTM"
        ]
        var components = URLComponents(string: baseUrl + searchPrefix)!
        components.queryItems = params.map{ URLQueryItem(name: $0, value: $1) }
        return components.url!
    }
    
    private func answersQueryUrl(forQID id: Int) -> URL {
        let answersPrefix = "/questions/\(String(id))/answers?"
        let params = [
            "order" : "desc",
            "sort" : "activity",
            "site" : "stackoverflow",
            "filter" : "!9YdnSLiq6"
        ]
        var components = URLComponents(string: baseUrl + answersPrefix)!
        components.queryItems = params.map{ URLQueryItem(name: $0, value: $1) }
        return components.url!
    }
    
    typealias QuestionSearchRequestCompletion = (_ result: QuestionsResult?,
                                                 _ error: DataMangerError?) -> Void
    
    func search(q: String, page: Int = 1, completion: QuestionSearchRequestCompletion?) {
        let session = URLSession.shared
        
        let url = searchQuestionsUrl(q: q, page: page)
        let task = session.dataTask(with: url) {[unowned self] (data, responce, error) in
            if error != nil {
                completion?(nil, DataMangerError.downloadError)
                return
            }
            
            guard let data = data else {
                completion?(nil, DataMangerError.decodingError)
                return
            }
            
            do {
                let result = try self.jsonDecoder.decode(QuestionsResult.self, from: data)
                completion?(result, nil)
            }
            catch let er {
                completion?(nil, DataMangerError.decodingError)
                print(er.localizedDescription)
            }
        }
        task.resume()
    }
    
    typealias AnswersRequestCompletion = (_ result: AnswersResult?,
                                          _ error: DataMangerError?) -> Void
    
    func getAnswers(forQ qID: Int, completion: AnswersRequestCompletion?) {
        let session = URLSession.shared
        
        let url = answersQueryUrl(forQID: qID)
        let task = session.dataTask(with: url) {[unowned self] (data, responce, error) in
            if error != nil {
                completion?(nil, DataMangerError.downloadError)
                return
            }
            
            guard let data = data else {
                completion?(nil, DataMangerError.decodingError)
                return
            }
            
            do {
                let res = try self.jsonDecoder.decode(AnswersResult.self, from: data)
                completion?(res, nil)
            }
            catch let er {
                completion?(nil, DataMangerError.decodingError)
                print(er.localizedDescription)
            }
        }
        task.resume()
    }
    
}

