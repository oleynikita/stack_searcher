//
//  ViewController.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/12/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    fileprivate var collectionManager: QuestionCollectionViewManager!
    @IBOutlet weak var searchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionManager = QuestionCollectionViewManager(collectionView: collectionView)
        collectionManager.delegate = self
        searchBar.delegate = self
        setupNavigationBar()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.barTintColor = UIColor(red:0.10, green:0.62, blue:0.82, alpha:1.0)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.tintColor = UIColor.black
        let textAttributes = [NSAttributedStringKey.foregroundColor:UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
    }
}

extension SearchViewController: QuestionCollectionViewManagerDelegate {
    func didSelect(question: Question, at indexPath: IndexPath) {
        if let vc = UIStoryboard.initialVC(ofType: .detail) as? QuestionViewController {
            vc.question = question
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func errorOccured(error: Error) {
        showInfo(title: "error", message: error.localizedDescription)
    }
}

extension SearchViewController: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let text = searchBar.text {
            collectionManager.currentSearchQuery = text
            collectionManager.search()
        }
    }
}
