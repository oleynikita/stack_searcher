//
//  QuestionsTableViewManager.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/12/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import HTMLString

protocol QuestionCollectionViewManagerDelegate: class {
    func didSelect(question: Question, at indexPath: IndexPath)
    func errorOccured(error: Error)
}

class QuestionCollectionViewManager: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    var dataItems: [Question] = []
    let dataManager = SOFDataManager()
    private let collectionView: UICollectionView
    weak var delegate: QuestionCollectionViewManagerDelegate?
    
    var shouldLoad: Bool = true
    var hidesFooter: Bool = true
    var currentSearchQuery: String = ""
    var footerState: LoadingFooterState = .empty
    fileprivate var currentQuestionsResult: QuestionsResult?
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        super.init()
        
        collectionView.registerCell(of: .question)
        collectionView.registerSupplimentary(ofType: .loadingFooter, ofKind: .footer)
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
    func search() {
        dataManager.search(q: currentSearchQuery) { (result, error) in
            if error != nil {
                self.delegate?.errorOccured(error: error!)
            }
            else {
                self.currentQuestionsResult = result
                self.dataItems = result?.items ?? []
                self.reloadAsync()
            }
        }
    }
}

// collection view
extension QuestionCollectionViewManager {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeCell(type: .question, for: indexPath) as! QuestionCell
        let item = dataItems[indexPath.item]
        cell.configCell(with: item)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = 200.0 as CGFloat
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let question = dataItems[indexPath.item]
        delegate?.didSelect(question: question, at: indexPath)
    }
    
    func reloadAsync() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let hasMore = currentQuestionsResult?.hasMore ?? false
        if !shouldLoad || !hasMore {
            footerState = .noMore
            return
        }
        if indexPath.item == (dataItems.count-1) {
            self.shouldLoad = false
            hidesFooter = false
            footerState = .loading
            loadNextPage()
        }
    }
    
    private func loadNextPage() {
        dataManager.search(q: currentSearchQuery, page: (currentQuestionsResult?.page)! + 1, completion: { (result, error) in
            self.shouldLoad = true
            self.hidesFooter = true
            
            if error != nil {
                self.delegate?.errorOccured(error: error!)
            }
            else {
                self.currentQuestionsResult = result
                self.dataItems.append(contentsOf: result?.items ?? [])
                self.reloadAsync()
            }
        })
    }
        
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let footer = collectionView.dequeueReusableSupplementaryView(ofKind: SupplimentaryKind.footer.rawValue, withReuseIdentifier: SupplimentaryType.loadingFooter.rawValue, for: indexPath)
        (footer as? LoadingFooter)?.state = footerState
        return footer
    }
}
