//
//  QuestionViewController.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit
import HTMLString

class QuestionViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    let dataManager = SOFDataManager()
    var collectionManager: AnswersCollectionManager!
    var question: Question?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationItem.hidesBackButton = false
        guard let question = question else { return }
        collectionManager = AnswersCollectionManager(collectionView: collectionView)
        collectionManager.question = question
        dataManager.getAnswers(forQ: question.id) { (result, error) in
            DispatchQueue.main.async {
                self.collectionManager.dataItems = result?.items ?? []
            }
        }
    }
}
