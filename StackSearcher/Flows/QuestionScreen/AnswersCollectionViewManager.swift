//
//  AnswersCollectionViewManager.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/19/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit

class AnswersCollectionManager: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var question: Question? = nil
    private let collectionView: UICollectionView
    
    var dataItems: [Answer] = [] {
        didSet { collectionView.reloadData() }
    }
    
    init(collectionView: UICollectionView) {
        self.collectionView = collectionView
        
        super.init()
        
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.registerCell(of: .answer)
        collectionView.registerSupplimentary(ofType: .questionHeader, ofKind: .header)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataItems.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeCell(type: .answer, for: indexPath) as! AnswerCell
        let answer = dataItems[indexPath.item]
        cell.config(with: answer)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeSupplimentary(type: .questionHeader, ofKind: kind, for: indexPath) as! QuestionHeader
        
        let item = question!
        header.config(with: item)
        
        return header
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let width = collectionView.bounds.width
        

        
        let title = question?.title ?? ""
        let body = question?.body ?? ""
        
        let height = title.height(withConstrainedWidth: width - 32.0,
                                  font: UIFont.systemFont(ofSize: 17)) +
                     body.height(withConstrainedWidth: width - 32.0,
                                 font: UIFont.systemFont(ofSize: 17)) +
                     80.0
        
        let size = CGSize(width: width, height: height)
        
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let item = dataItems[indexPath.item]
        let height = item.body.height(withConstrainedWidth: width - 16, font: UIFont.systemFont(ofSize: 17)) + 60
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
}
