//
//  QuestionCell.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

class QuestionCell: UICollectionViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var questionDate: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var answers: UILabel!
    @IBOutlet weak var views: UILabel!
    @IBOutlet weak var bottomSection: UIStackView!
    @IBOutlet weak var checkIcon: UIImageView!
    
    let separatorShape = CAShapeLayer()
    
    func configCell(with item: Question) {
        self.title.text = item.title
        self.body.text = item.body.removingHTMLEntities
        self.votes.text = String(item.votes)
        self.answers.text = String(item.answers)
        self.views.text = String(item.views)
        self.userName.text = item.owner.name
        self.questionDate.text = Date.fromInt(value: item.date).dateString(for: .medium)
        if let ava = item.owner.avatarUrl,
            let url = URL(string: ava) {
            self.avatar.sd_setImage(with: url, completed: nil)
        }
        self.checkIcon.isHidden = !item.isAnswered
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        avatar.clipsToBounds = true
        initUI()
        setupUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }
    
    func initUI() {
        self.layer.addSublayer(separatorShape)
        self.separatorShape.fillColor = UIColor.lightGray.withAlphaComponent(0.2).cgColor
        setupBottomSeparator()
    }
    
    fileprivate func setupAvatarView() {
        avatar.layer.cornerRadius = avatar.bounds.width / 2
        avatar.layer.borderWidth = 0.5
        avatar.layer.borderColor = UIColor.lightGray.cgColor
    }
    
    func setupUI() {
        setupAvatarView()
        setupDropShadow()
        setupBottomSeparator()
    }
    
    func setupBottomSeparator() {
        let rect = CGRect(x: bounds.minX, y: bottomSection.frame.minY,
                          width: bounds.width, height: 1)
        let path = UIBezierPath(rect: rect)
        separatorShape.path = path.cgPath
    }

}
