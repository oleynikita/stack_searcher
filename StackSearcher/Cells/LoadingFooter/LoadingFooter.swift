//
//  LoadingFooter.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/18/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

enum LoadingFooterState {
    case loading
    case empty
    case noMore
    
    var message: String {
        switch self {
        case .loading:
            return "Loading.."
        case .empty:
            return "No results"
        case .noMore:
            return "No more results"
        }
    }
}

class LoadingFooter: UICollectionReusableView {

    var state: LoadingFooterState = .empty {
        didSet {
            switch state {

            case .loading:
                activity.startAnimating()
                infoText.isHidden = true
            case .empty:
                activity.stopAnimating()
                infoText.isHidden = false
                infoText.text = state.message
            case .noMore:
                activity.stopAnimating()
                infoText.isHidden = false
                infoText.text = state.message
            }
        }
    }
    
    @IBOutlet weak var infoText: UILabel!
    @IBOutlet weak var activity: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        state = .empty
    }
    
}
