//
//  AnswerCell.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

class AnswerCell: UICollectionViewCell {

    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var checkIcon: UIImageView!
    @IBOutlet weak var votes: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        setupUI()
        avatar.clipsToBounds = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupUI()
    }
    
    func config(with item: Answer) {
        self.name.text = item.owner.name
        if let ava = item.owner.avatarUrl,
            let url = URL(string: ava) {
            self.avatar.sd_setImage(with: url, completed: nil)
        }
        self.body.text = item.body.removingHTMLEntities
        self.checkIcon.isHidden = !item.isAccepted
        self.votes.text = String(item.votes)
    }
    
    func setupUI() {
        setupDropShadow()
        setupAvatarView()
    }
    
    fileprivate func setupAvatarView() {
        avatar.layer.cornerRadius = avatar.bounds.width / 2
        avatar.layer.borderWidth = 0.5
        avatar.layer.borderColor = UIColor.lightGray.cgColor
    }
}
