//
//  QuestionHeaderCollectionReusableView.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/14/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

class QuestionHeader: UICollectionReusableView {

    @IBOutlet weak var ava: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var body: UILabel!
    @IBOutlet weak var votes: UILabel!
    @IBOutlet weak var answers: UILabel!
    @IBOutlet weak var views: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        ava.clipsToBounds = true
        // Initialization code
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        ava.layer.cornerRadius = ava.bounds.width / 2
    }
    
    func config(with item: Question) {
        self.title.text = item.title
        self.body.text = item.body.removingHTMLEntities
        self.votes.text = String(item.votes)
        self.answers.text = String(item.answers)
        self.views.text = String(item.views)
        self.name.text = item.owner.name
        let date = Date.fromInt(value: item.date)
        self.date.text = date.dateString(for: .medium)
        if let ava = item.owner.avatarUrl,
            let url = URL(string: ava) {
            self.ava.sd_setImage(with: url, completed: nil)
        }
    }
    
}
