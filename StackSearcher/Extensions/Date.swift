//
//  Date.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

extension Date {
    func dateString(for style: DateFormatter.Style) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = style
        return formatter.string(from: self)
    }
    
    static func fromInt(value: Int) -> Date {
        return Date(timeIntervalSince1970: TimeInterval(value))
    }
    
}
