//
//  UITableView.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/12/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit

enum CellType: String {
    case question = "QuestionCell"
    case answer = "AnswerCell"
}

enum SupplimentaryType: String {
    case loadingFooter = "LoadingFooter"
    case questionHeader = "QuestionHeader"
}

enum SupplimentaryKind: String {
    case footer = "UICollectionElementKindSectionFooter"
    case header = "UICollectionElementKindSectionHeader"
}

extension UICollectionView {
    func registerCell(of type: CellType) {
        let nib = UINib(nibName: type.rawValue, bundle: nil)
        self.register(nib, forCellWithReuseIdentifier: type.rawValue)
    }
    
    func dequeCell(type: CellType, for indexPath: IndexPath) -> UICollectionViewCell {
        return self.dequeueReusableCell(withReuseIdentifier: type.rawValue, for: indexPath)
    }
    
    func registerSupplimentary(ofType type: SupplimentaryType, ofKind kind: SupplimentaryKind) {
        let nib = UINib(nibName: type.rawValue, bundle: nil)
        self.register(nib, forSupplementaryViewOfKind: kind.rawValue, withReuseIdentifier: type.rawValue)
    }
    
    func dequeSupplimentary(type: SupplimentaryType, ofKind kind: String, for indexPath: IndexPath) -> UICollectionReusableView {
        return self.dequeueReusableSupplementaryView(ofKind: kind,
                                                     withReuseIdentifier: type.rawValue,
                                                     for: indexPath)
    }
    
}
