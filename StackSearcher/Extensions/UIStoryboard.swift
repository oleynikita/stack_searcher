//
//  UIStoryboard.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit

enum VCType: String {
    case search = "SearchViewController"
    case detail = "QuestionViewController"
}

extension UIStoryboard {
    static func initialVC(ofType type: VCType) -> UIViewController? {
        return UIStoryboard(name: type.rawValue, bundle: nil).instantiateInitialViewController()
    }
}
