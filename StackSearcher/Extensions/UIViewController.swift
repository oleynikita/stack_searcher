//
//  UIViewController.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/19/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    func showInfo(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
