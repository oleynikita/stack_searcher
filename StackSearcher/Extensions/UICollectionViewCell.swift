//
//  UICollectionViewCell.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/19/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionViewCell {
    func setupDropShadow(scale: Bool = true) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 1, height: 1)
        self.layer.shadowRadius = 2
        
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
        self.layer.shouldRasterize = true
        self.layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
