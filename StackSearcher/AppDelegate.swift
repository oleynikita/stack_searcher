//
//  AppDelegate.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/12/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
        window?.backgroundColor = UIColor.white
        window?.makeKeyAndVisible()
        window?.rootViewController = UINavigationController(rootViewController: UIStoryboard.initialVC(ofType: .search)!)
        
        return true
    }
}

