//
//  Question.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

struct Question: Codable {
    
    let id: Int
    
    let owner: Owner
    let date: Int
    
    let title: String
    let body: String
    
    let votes: Int
    let answers: Int
    let views: Int
    
    let isAnswered: Bool
    
    enum CodingKeys: String, CodingKey {
        case id = "question_id"
        case date = "creation_date"
        case title = "title"
        case body = "body_markdown"
        case votes = "score"
        case answers = "answer_count"
        case views = "view_count"
        case isAnswered = "is_answered"
        case owner
    }
}
