//
//  Answer.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

struct Answer: Codable {
    let id: Int
    
    let owner: Owner
    let isAccepted: Bool
    let votes: Int
    let body: String
    
    
    enum CodingKeys: String, CodingKey {
        case id = "answer_id"
        case isAccepted = "is_accepted"
        case votes = "score"
        case body = "body_markdown"
        case owner
    }
}
