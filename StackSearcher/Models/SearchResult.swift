//
//  SearchResult.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

struct QuestionsResult: Codable {
    
    let items: [Question]?
    let hasMore: Bool?
    let page: Int?
    
    enum CodingKeys: String, CodingKey {
        case items
        case hasMore = "has_more"
        case page
    }
    
}

struct AnswersResult: Codable {
    
    var items: [Answer]
    var hasMore: Bool
    //let page: Int
    
    enum CodingKeys: String, CodingKey {
        case items
        case hasMore = "has_more"
        //case page
    }
}
