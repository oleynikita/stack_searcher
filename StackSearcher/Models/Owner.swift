//
//  Owner.swift
//  StackSearcher
//
//  Created by Mykyta Oliinyk on 12/13/17.
//  Copyright © 2017 Mykyta Oliinyk. All rights reserved.
//

import Foundation

struct Owner: Codable {
    let name: String
    let avatarUrl: String?
    enum CodingKeys: String, CodingKey {
        case name = "display_name"
        case avatarUrl = "profile_image"
    }
}
